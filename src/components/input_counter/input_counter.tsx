import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../state/reducers/root";
import "./input_counter.scss";
import { TimerTypes } from "../../constants/enums";

function InputCounter() {
  const [text, setText] = useState<string>("");
  const [number, setNumber] = useState<number>(0);
  const timer = useSelector((state: RootState) => state.timer.value);

  useEffect(() => {
    if (!text.length) {
      setNumber(0);
      return;
    }
    let number = text.trim().split(" ");
    setNumber(number.length);
  }, [text]);

  const handleTextChange = (e: any) => {
    let text = e.target.value;
    setText(text);
  };

  return (
    <div className="input-counter">
      <textarea
        disabled={timer.type !== TimerTypes.Test}
        className={timer.type === TimerTypes.Test ? " start" : ""}
        value={text}
        onChange={(e) => handleTextChange(e)}
        placeholder="Text here..."
      ></textarea>
      <small>Words: {number}</small>
    </div>
  );
}

export default InputCounter;
