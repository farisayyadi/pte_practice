import React from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import ENDPOINTS from "../../constants/endpoints";
import { RootState } from "../../state/reducers/root";
import { change_bookmark } from "../../state/reducers/question";
import "./bookmark.scss";

function Bookmark() {
  const user = useSelector((state: RootState) => state.user.value);
  const question = useSelector((state: RootState) => state.question.value);
  const dispatch = useDispatch();

  function changeTag(bookmark: number) {
    axios
      .post(
        ENDPOINTS.change_bookmark,
        {
          questionNumber: question.number,
          category: question.category,
          bookmark: bookmark,
        },
        {
          headers: {
            Authorization: `Bearer ${user.token}`,
          },
        }
      )
      .then((res) => {
        const bookmark = res.data;
        dispatch(change_bookmark(bookmark));
      });
  }

  return (
    <span className='bookmark'>
      <i id={"_" + question?.bookmark} className='las la-bookmark'></i>
      <span>
        {[1, 2, 3, 4, 5].map((bookmark: number, idx: number) => {
          if (bookmark === question?.bookmark) return "";
          return (
            <i
              key={idx}
              id={"_" + bookmark}
              onClick={() => changeTag(bookmark)}
              className='las la-bookmark'></i>
          );
        })}
      </span>
    </span>
  );
}

export default Bookmark;
