import React, { useEffect } from "react";
import BtnFillet from "../btn_fillet/btn_fillet";
import { RootState } from "../../state/reducers/root";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import getQuestionData from "../../utils/questionLoader";
import { update_question } from "../../state/reducers/question";
import "./navigator.scss";
import { delay_start, reset_timer } from "../../state/reducers/timer";

function Navigator() {
  const { category } = useParams();
  const dispatch = useDispatch();
  const { number, total, answer } = useSelector(
    (state: RootState) => state.question.value
  );
  const timer = useSelector((state: RootState) => state.timer.value);
  const user = useSelector((state: RootState) => state.user.value);

  useEffect(() => {
    if (category) {
      loadQuestion(1);
    }
  }, [category]);

  async function loadQuestion(questionNumber: number) {
    if (questionNumber === number || !category) {
      return;
    }

    try {
      const question = await getQuestionData(category, questionNumber);

      if (question && question.length > 0) {
        dispatch(update_question({ question: question[0] }));
      } else {
        console.error("Empty or invalid question data.");
      }
    } catch (error) {
      console.error("Error fetching question data:", error);
    }
  }

  function prevQuestion() {}

  function nextQuestion() {
    if (number) {
      loadQuestion(number + 1);
      dispatch(reset_timer());
      dispatch(delay_start({ number: 4 }));
    }
  }

  function submitAnswer() {
    let data = {
      user: {},
      question: {
        question: number,
        category,
        answer: answer,
      },
    };
    console.log("submit");
    console.log(data);
    // send answer to db
  }

  function isTestDone() {
    return timer.done;
  }

  return (
    <div id="navigator">
      <div>
        <BtnFillet green disable={!isTestDone()} click={submitAnswer}>
          Submit
        </BtnFillet>
        <BtnFillet green disable={!isTestDone()}>
          Re-do
        </BtnFillet>
      </div>
      <div>
        <BtnFillet
          green
          strok
          disable={number === 1 || !isTestDone()}
          click={prevQuestion}
        >
          prev
        </BtnFillet>
        <BtnFillet
          green
          strok
          disable={number === total || !isTestDone()}
          click={nextQuestion}
        >
          next
        </BtnFillet>
      </div>
      <div>
        <BtnFillet blue strok disable={!user.chips}>
          <i className="las la-book"></i>
          Dict Mode
        </BtnFillet>
        <BtnFillet red disable={!user.chips}>
          <i className="las la-ticket-alt" />
          {user.chips}
        </BtnFillet>
      </div>
    </div>
  );
}

export default Navigator;
