import React, { useEffect, useState } from "react";
import "./select.scss";

interface SelectModel {
  preSelected?: any;
  options: any[];
  onChange: (key: any) => void;
}

const Select = ({ options, preSelected, onChange }: SelectModel) => {
  const [selectedItem, setSelectedItem] = useState();

  useEffect(() => {
    if (preSelected) {
      setSelectedItem(preSelected);
    } else {
      setSelectedItem(options[0]);
    }
  }, []);

  function toggleSelection(itemIdx: number) {
    const selectedItem = options[itemIdx];
    setSelectedItem(selectedItem);
    onChange(selectedItem);
  }

  return (
    <span className="select">
      <span>{selectedItem}</span>
      <i className="las la-caret-down"></i>
      <ul>
        {options.map((item: any, idx: number) => (
          <li
            key={idx}
            onClick={() => toggleSelection(idx)}
            className={item === selectedItem ? "selected" : ""}
          >
            {item}
          </li>
        ))}
      </ul>
    </span>
  );
};

export default Select;
