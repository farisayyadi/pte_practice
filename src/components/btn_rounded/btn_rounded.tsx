import React from "react";
import { Link } from "react-router-dom";
import classNames from "classnames";
import { BtnRoundedModel } from "./model";
import "./btn_rounded.scss";

function BtnRounded({
  green,
  strock,
  link,
  disable,
  children,
}: BtnRoundedModel) {
  const styles = classNames("btn", "rounded", "shadow", {
    green,
    strock,
    disable,
  });
  if (link) {
    return (
      <Link className={styles} to={!disable ? link : ""}>
        {children}
      </Link>
    );
  }

  return (
    <button className={styles} disabled={disable}>
      {children}
    </button>
  );
}

export default BtnRounded;
