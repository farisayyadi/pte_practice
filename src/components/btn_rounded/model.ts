export interface BtnRoundedModel {
  green: boolean;
  strock: boolean;
  link: string;
  disable?: boolean;
  children: any;
}
