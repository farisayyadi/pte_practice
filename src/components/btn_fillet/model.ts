export interface BtnFilletModel {
  green?: boolean;
  blue?: boolean;
  red?: boolean;
  gray?: boolean;
  small?: boolean;
  to?: string;
  disable?: boolean;
  hide?: boolean;
  click?: () => any;
  strok?: boolean;
  children: any;
}
