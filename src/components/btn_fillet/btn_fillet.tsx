import React from "react";
import { Link } from "react-router-dom";
import classNames from "classnames";
import { BtnFilletModel } from "./model";
import "./btn_fillet.scss";

const BtnFillet = ({
  green,
  blue,
  red,
  gray,
  small,
  strok,
  to,
  disable,
  hide,
  click,
  children,
}: BtnFilletModel) => {
  const buttonClassNames = classNames("btn", "fillet", "shadow", {
    green,
    blue,
    red,
    gray,
    small,
    strok,
    hide,
    click,
  });

  if (to) {
    return (
      <Link
        className={buttonClassNames}
        onClick={click}
        to={!disable ? to : ""}
      >
        {children}
      </Link>
    );
  }

  return (
    <button
      className={buttonClassNames}
      disabled={disable}
      onClick={click}
      type="button"
    >
      {children}
    </button>
  );
};

export default BtnFillet;
