import React from "react";
import "./App.scss";
import { BrowserRouter as Router } from "react-router-dom";
import Header from "./components/header/header";
import Content from "./components/content/content";
import Footer from "./components/footer/footer";
import GoUp from "./components/goup/goup";
import { Provider } from "react-redux";
import store from "./state/reducers/root";

export default function App() {
  return (
    <Provider store={store}>
      <Router>
        <Header />
        <Content />
        <Footer />
        <GoUp />
      </Router>
    </Provider>
  );
}
